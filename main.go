package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	var contacts *Contacts

	// initialize contacts
	filename := "contacts.json"
	c, err := ContactsFromFile(filename)
	if err != nil {
		fmt.Printf("Error loading Contacts: %v \n", err)
		fmt.Println("Setting up new data... ")
		contacts = NewContacts(filename)
	} else {
		contacts = c
	}

	// Static assets
	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	http.Handle("/{$}", http.RedirectHandler("/contacts", 301))
	http.Handle("/contacts", CreateDisplayContactsHandler(contacts))
	http.Handle("POST /contacts/multi-delete", CreateContactMultiDeleteHandler(contacts))
	http.Handle("GET /contacts/count", CreateContactCountHandler(contacts))
	new_handler := CreateNewContactHandler(contacts)
	http.Handle("GET /contacts/new", new_handler)
	http.Handle("POST /contacts/new", new_handler)
	http.Handle("/contacts/{id}", CreateContactViewHandler(contacts))
	http.Handle("/contacts/{id}/edit", CreateContactEditHandler(contacts))
	http.Handle("GET /contacts/{id}/email", CreateEmailValidateHandler(contacts))
	http.Handle("DELETE /contacts/{id}", CreateContactDeleteHandler(contacts))

	log.Fatal(http.ListenAndServe(":8080", nil))
}
