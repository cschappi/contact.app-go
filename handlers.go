package main

import (
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"strconv"
)

type DisplayContactsHandler struct {
	tmpl     *template.Template
	contacts *Contacts
}
type contactsData struct {
	Query    string
	Page     int
	MaxPage  int
	Contacts []Contact
}

const numPerPage = 10

func (self DisplayContactsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	search, ok := r.URL.Query()["q"]
	var data contactsData
	if ok {
		data.Contacts = self.contacts.Search(search[0])
		data.Query = search[0]
	} else {
		data.Contacts = self.contacts.All()
	}
	numContacts := len(data.Contacts)
	numPages := ((numContacts - 1) / numPerPage) + 1
	data.MaxPage = numPages

	if page, ok := r.URL.Query()["page"]; ok {
		if num, err := strconv.Atoi(page[0]); err == nil {
			data.Page = num
		}
	} else {
		data.Page = 1
	}

	data.Contacts = data.Contacts[(data.Page-1)*10 : min(data.Page*10, numContacts)]

	if r.Header.Get("HX-Trigger") == "search" {
		// Active search request
		if err := self.tmpl.ExecuteTemplate(w, "contacts-rows", data); err != nil {
			log.Println(err)
		}
	} else {
		if err := self.tmpl.Execute(w, data); err != nil {
			log.Println(err)
		}
	}
}
func CreateDisplayContactsHandler(contacts *Contacts) DisplayContactsHandler {
	tmpl := template.Must(template.New("layout.html").Funcs(template.FuncMap{
		"incr": func(x int) int {
			return x + 1
		},
		"decr": func(x int) int {
			return x - 1
		},
	}).ParseFiles(templateFilesList("contacts")...))
	fmt.Println("Parsed files: ", templateFilesList("contacts"))
	return DisplayContactsHandler{tmpl, contacts}
}

type ContactCountHandler struct {
	contacts *Contacts
}

func (self ContactCountHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, fmt.Sprintf("(%v total contacts)", self.contacts.Count()))
}
func CreateContactCountHandler(contacts *Contacts) ContactCountHandler {
	return ContactCountHandler{contacts}
}

type NewContactHandler struct {
	tmpl     *template.Template
	contacts *Contacts
}

func (self NewContactHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		self.tmpl.Execute(w, EmptyContact())
	} else if r.Method == "POST" {
		r.ParseForm()

		new_contact := NewContactWithoutId(r.Form.Get("first_name"), r.Form.Get("last_name"), r.Form.Get("email"), r.Form.Get("phone"))
		err := self.contacts.Insert(new_contact)
		if err != nil {
			// Retry the form
			self.tmpl.Execute(w, new_contact)
		}

		if err := self.contacts.Save(); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			io.WriteString(w, fmt.Sprintf("Error occured: %v", err))
		}

		// Success, redirect to contact page
		http.Redirect(w, r, "/contacts", 303)
	}
}
func CreateNewContactHandler(contacts *Contacts) NewContactHandler {
	return NewContactHandler{
		tmpl:     template.Must(templateWithLayout("new")),
		contacts: contacts,
	}
}

type ContactViewHandler struct {
	tmpl     *template.Template
	contacts *Contacts
}

func (self ContactViewHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	contact := GetContactFromHttp(self.contacts, w, r)
	if contact == nil {
		return
	}
	self.tmpl.Execute(w, contact)
}
func CreateContactViewHandler(contacts *Contacts) NewContactHandler {
	return NewContactHandler{
		tmpl:     template.Must(templateWithLayout("show")),
		contacts: contacts,
	}
}

type ContactEditHandler struct {
	tmpl     *template.Template
	contacts *Contacts
}
type editData struct {
	Contact Contact
	Error   string
}

func (self ContactEditHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		contact := GetContactFromHttp(self.contacts, w, r)
		if contact == nil {
			return
		}
		data := editData{
			*contact, "",
		}
		self.tmpl.Execute(w, data)
	} else if r.Method == "POST" {
		r.ParseForm()
		contact := GetContactFromHttp(self.contacts, w, r)
		if contact == nil {
			return
		}
		contact.Update(r.PostForm.Get("first_name"), r.PostForm.Get("last_name"), r.PostForm.Get("email"), r.PostForm.Get("phone"))
		err := contact.Validate(self.contacts.All())
		if err != nil {
			// error occured
			data := editData{
				*contact, fmt.Sprintf("Data error: %v", err),
			}
			self.tmpl.Execute(w, data)
			return
		}

		if err := self.contacts.Save(); err != nil {
			data := editData{
				*contact, fmt.Sprintf("Error while storing contact: %v", err),
			}
			self.tmpl.Execute(w, data)
		}

		http.Redirect(w, r, "/contacts", 303)
	}
}
func CreateContactEditHandler(contacts *Contacts) ContactEditHandler {
	return ContactEditHandler{
		tmpl:     template.Must(templateWithLayout("edit")),
		contacts: contacts,
	}
}

type ContactDeleteHandler struct {
	contacts *Contacts
}

func (self ContactDeleteHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	contact_id := r.PathValue("id")
	id, err := strconv.Atoi(contact_id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	self.contacts.DeleteById(id)

	if r.Header.Get("HX-Trigger") == "delete-btn" {
		http.Redirect(w, r, "/contacts", 303)
	}
	// Do nothing, return empty string
}
func CreateContactDeleteHandler(contacts *Contacts) ContactDeleteHandler {
	return ContactDeleteHandler{
		contacts: contacts,
	}
}

type ContactMultiDeleteHandler struct {
	tmpl     *template.Template
	contacts *Contacts
}

func (self ContactMultiDeleteHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	contact_ids := r.PostForm["selected_contact_ids"]
	for _, id_str := range contact_ids {
		id, err := strconv.Atoi(id_str)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		self.contacts.DeleteById(id)
	}

	http.Redirect(w, r, "/contacts", 303)
}
func CreateContactMultiDeleteHandler(contacts *Contacts) ContactMultiDeleteHandler {
	return ContactMultiDeleteHandler{
		contacts: contacts,
	}
}

type EmailValidateHandler struct {
	contacts *Contacts
}

func (self EmailValidateHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	contact := *GetContactFromHttp(self.contacts, w, r)
	contact.Email = r.URL.Query().Get("email")
	err := contact.Validate(self.contacts.All())
	resp := ""
	if err != nil {
		resp = fmt.Sprintf("%v", err)
	}
	io.WriteString(w, resp)
}
func CreateEmailValidateHandler(contacts *Contacts) EmailValidateHandler {
	return EmailValidateHandler{
		contacts: contacts,
	}
}

func templateWithLayout(names ...string) (*template.Template, error) {
	return template.ParseFiles(templateFilesList(names...)...)
}
func templateFilesList(names ...string) []string {
	list := make([]string, len(names)+1)
	list[0] = "template/layout.html"
	for i, name := range names {
		list[i+1] = fmt.Sprintf("template/%v.html", name)
	}
	return list
}
func GetContactFromHttp(contacts *Contacts, w http.ResponseWriter, r *http.Request) *Contact {
	contact_id := r.PathValue("id")
	id, err := strconv.Atoi(contact_id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return nil
	}
	contact := contacts.FindById(id)
	if contact == nil {
		http.NotFound(w, r)
		return nil
	}
	return contact
}
