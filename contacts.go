package main

import (
	"encoding/json"
	"errors"
	"os"
	"strings"
	"time"
)

type Contact struct {
	Id    int    `json:"id"`
	First string `json:"first"`
	Last  string `json:"last"`
	Email string `json:"email"`
	Phone string `json:"phone"`
}

func EmptyContact() Contact {
	return Contact{
		0, "", "", "", "",
	}
}
func NewContactWithoutId(
	First string,
	Last string,
	Email string,
	Phone string,
) Contact {
	return Contact{
		-1, First, Last, Email, Phone,
	}
}

func (self *Contact) Update(First, Last, Email, Phone string) {
	self.First = First
	self.Last = Last
	self.Email = Email
	self.Phone = Phone
}
func (self *Contact) Validate(others []Contact) error {
	if self.First == "" || self.Last == "" || self.Email == "" || self.Phone == "" {
		return errors.New("No contact field may be empty")
	}
	for _, c := range others {
		if c.Id != self.Id && c.Email == self.Email {
			return errors.New("Contact Emails must be unique")
		}
	}
	return nil
}

type Contacts struct {
	List     []Contact
	Filename string
}

func NewContacts(fname string) *Contacts {
	return &Contacts{[]Contact{
		{Id: 0, First: "John", Last: "Doe", Email: "john.doe@gmail.com", Phone: "12345678"},
		{Id: 1, First: "Clara", Last: "Doe", Email: "clara.doe@gmail.com", Phone: "0-278-3052"},
		{Id: 2, First: "Max", Last: "Mustermann", Email: "max.mustermann@gmx.de", Phone: "0177987654"},
	},
		fname}
}
func ContactsFromFile(fname string) (*Contacts, error) {
	f, err := os.Open(fname)
	if err != nil {
		return nil, err
	}
	decoder := json.NewDecoder(f)
	var contacts []Contact
	if err := decoder.Decode(&contacts); err != nil {
		return nil, err
	}
	return &Contacts{contacts, fname}, nil
}

func (self *Contacts) All() []Contact {
	return self.List
}
func (self *Contacts) Search(query string) []Contact {
	result := make([]Contact, 0)
	for _, contact := range self.List {
		if strings.Contains(contact.Last, query) {
			result = append(result, contact)
		}
	}
	return result
}
func (self *Contacts) FindById(id int) *Contact {
	for i, contact := range self.List {
		if contact.Id == id {
			return &self.List[i]
		}
	}
	return nil
}
func (self *Contacts) DeleteById(id int) {
	for i, contact := range self.List {
		if contact.Id == id {
			self.List = append(self.List[:i], self.List[i+1:]...)
		}
	}
}
func (self *Contacts) Insert(contact Contact) error {
	err := contact.Validate(self.List)
	if err != nil {
		return err
	}
	// all valid data
	id := self.List[len(self.List)-1].Id + 1
	contact.Id = id
	self.List = append(self.List, contact)
	return nil
}

func (self *Contacts) Save() error {
	f, err := os.Create(self.Filename)
	if err != nil {
		return err
	}
	enc := json.NewEncoder(f)
	if err := enc.Encode(self.List); err != nil {
		return err
	}
	return nil
}

func (self *Contacts) Count() int {
	time.Sleep(2 * time.Second)
	return len(self.List)
}
